package com.proyecto.calculadoraapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.text.DecimalFormat;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class MainActivity extends AppCompatActivity {
    private TextView panel;
    private TextView result;
    private String query;
    private static DecimalFormat REAL_FORMATTER = new DecimalFormat("0.###");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        query = "";

        panel = findViewById(R.id.textView);
        panel.setText("");

        result = findViewById(R.id.textViewResult);
        result.setText("");
    }

    // text view
    public void setTextView(View view) {
        Button b = (Button) findViewById(view.getId());
        panel.append(b.getText());
    }

    // Este metodo realiza la operacion con Javascript
    public String calculateResult(String cadena) {
        // Crea Engine Javascript
        try {
            ScriptEngineManager mgr = new ScriptEngineManager();
            ScriptEngine engine = mgr.getEngineByName("js");
            Double auxResult = (double) engine.eval(cadena);
            cadena = REAL_FORMATTER.format(auxResult);

            System.out.println("result  = " + auxResult);
        } catch (ScriptException e) {
            cadena = getString(R.string.syserror);
            e.printStackTrace();
        }

        return cadena;
    }

    // @ button event
    public void buttonIgualAction(View view) {
        query = panel.getText().toString();
        query = this.calculateResult(query);
        result.setText(query);
    }

    // @ button event
    public void buttonAboutAction(View view) {
        startActivity(new Intent(MainActivity.this, AboutActivity.class));
    }

    // @ button event CE
    public void buttonCEAction(View view) {
        this.panel.setText("");
    }

    // @ button event <-
    public void buttonBorrarAction(View view) {
        String aux = this.panel.getText().toString();
        aux = aux.substring(0,aux.length()-1);
        this.panel.setText(aux);
    }
}
